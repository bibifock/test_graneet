CREATE ROLE webapp LOGIN;
CREATE DATABASE graneet;
GRANT ALL PRIVILEGES ON DATABASE graneet TO webapp;

\c graneet

CREATE TABLE IF NOT EXISTS city (
  codePostal TEXT NOT NULL,
  codeCommune TEXT NOT NULL,
  nomCommune TEXT NOT NULL,
  libelleAcheminement TEXT NOT NULL,
  CONSTRAINT city_pkey PRIMARY KEY (codePostal, codeCommune)
);

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO webapp;

\COPY city FROM '/var/postgres-data/codes-postaux.csv' CSV HEADER;
