import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', async () => {
    const { body } = await request(app.getHttpServer()).get('/').expect(200);

    expect(body.cities).toHaveLength(100);
    expect(body.total).toBeGreaterThan(100);
  });

  it('/?s=maurecourt (GET) return matched town', async () => {
    const { body } = await request(app.getHttpServer())
      .get('/?s=maurecourt')
      .expect(200);

    expect(body.total).toEqual(1);
    expect(body.cities).toHaveLength(1);
    expect(body.cities[0].codePostal).toEqual('78780');
  });

  it('/?s=m (GET), no more than 100 results', async () => {
    const { body } = await request(app.getHttpServer())
      .get('/?s=m')
      .expect(200);

    expect(body.total).toBeGreaterThan(100);
    expect(body.cities).toHaveLength(100);
  });
});
