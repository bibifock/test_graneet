import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { GetCitiesResponse } from './types';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getCities(@Query() query?: { s?: string }): Promise<GetCitiesResponse> {
    return this.appService.getCities(query?.s);
  }
}
