import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'city' })
export class CityEntity {
  @PrimaryColumn({ name: 'codepostal' })
  codePostal: string;

  @PrimaryColumn({ name: 'codecommune' })
  codeCommune: string;

  @Column({ name: 'nomcommune' })
  nomCommune: string;

  @Column({ name: 'libelleacheminement' })
  libelleAcheminement: string;
}
