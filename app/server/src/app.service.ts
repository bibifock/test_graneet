import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Raw, Like, FindManyOptions } from 'typeorm';

import { CityEntity } from './entity/city.entity';
import { GetCitiesResponse } from './types';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(CityEntity)
    private cityRepo: Repository<CityEntity>,
  ) {}

  getCities(search?: string): Promise<GetCitiesResponse> {
    const args: FindManyOptions<CityEntity> = {
      take: 100,
      order: {
        nomCommune: 'ASC',
        codePostal: 'ASC',
      },
    };

    if (search?.length) {
      args.where = [
        {
          nomCommune: Raw(
            (alias) => `LOWER(${alias}) Like '%${search.toLowerCase()}%'`,
          ),
        },
        { codePostal: Like(`%${search}%`) },
      ];
    }

    return this.cityRepo
      .findAndCount(args)
      .then(([cities, total]) => ({ cities, total }));
  }
}
