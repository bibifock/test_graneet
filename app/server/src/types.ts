export type City = {
  codePostal: string;
  codeCommune: string;
  nomCommune: string;
  libelleAcheminement: string;
};

export type GetCitiesResponse = {
  cities: City[];
  total: number;
};
