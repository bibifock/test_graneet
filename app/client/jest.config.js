// jest.config.js
module.exports = {
  testEnvironment: 'jest-environment-jsdom',
  setupFiles: ['<rootDir>/jest/setupJest'],
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.jsx?$": "babel-jest",
  },
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/jest/fileMock.js",
    "\\.(css|sass)$": "<rootDir>/jest/styleMock.js",
    "^@/(.*)$": "<rootDir>/src/$1"
  },
  globals: {
    'ts-jest': {
      isolatedModules: true,
    }
  }
};
