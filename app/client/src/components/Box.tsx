import styled from 'styled-components';

const Box = styled.div`
  background: #D2E5E9;
  border-radius: 10px;
  padding: 20px;
`;

export default Box;
