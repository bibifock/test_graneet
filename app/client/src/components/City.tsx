import React, { FC } from 'react';
import styled from 'styled-components';

const CityDiv = styled.div`
  background: rgba(22, 28, 41, 0.63);
  display: flex;
  align-items: center;
  min-width: 300px;
  padding: 20px;
`;

const Name = styled.span`
  color: #FFF;
  flex-grow: 1;
`;

const PostalCode = styled.span`
  color: #8C8F9A;
  flex-shrink: 0;
`;

export type CityProps = {
  nomCommune: string;
  codePostal: string;
}

const City: FC<CityProps> = ({ nomCommune, codePostal, ...rest }) => (
  <CityDiv {...rest}>
    <Name className='title'>{nomCommune}</Name>
    <PostalCode className='subtitle'>{codePostal}</PostalCode>
  </CityDiv>
);

export default City;

