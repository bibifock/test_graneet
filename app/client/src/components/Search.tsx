import React, { FC, ChangeEvent, useMemo, useState } from 'react';
import styled from 'styled-components';
import debounce from 'lodash/debounce';

import Box from '@/components/Box';

const Label = styled.label`
  color: #000;
`;

const SearchDiv = styled(Box)`
  display: flex;
  align-items: center;
  gap: 10px;

  label, input {
    font-family: 'Monserrat';
    font-size: 30px;
    color: #000;
  }

  label {
    flex-shrink: 0;
  }

  input {
    flex-grow: 1;
    background: #F0FCFF;
    border-radius: 10px;
    padding: 10px 25px;
    border: 0;

    &:focus {
      outline:none;
    }
  }

  input::placeholder {
    color: #DCDCDC;

  }
`;

export type SearchProps = {
  onSearch: (search: string) => void;
};

const Search:FC<SearchProps> = ({ onSearch }) => {
  const [inputValue, setInputValue] = useState('');

  const handleChange = useMemo(
    () => {
      const handleSearch = debounce((search) => onSearch(search), 500);

      return (e: ChangeEvent<HTMLInputElement>) => {
        const search = e.currentTarget.value;

        setInputValue(search || '');
        handleSearch(search);
      };
    },
    [onSearch]
  );

  return (
    <SearchDiv className='container'>
      <label htmlFor='search_filter'>Je recherche...</label>
      <input
        id='search_filter'
        type='text'
        placeholder='...une ville, un code postal'
        value={inputValue}
        onChange={handleChange}
      />
    </SearchDiv>
  );
};

export default Search;
