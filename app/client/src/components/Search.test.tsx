import { create } from 'react-test-renderer';
import 'jest-styled-components';

import Search from './Search';

describe('components/search', () => {
  it('should render correctly', () =>  {
    const tree = create(<Search onSearch={jest.fn()} />);

    expect(tree.toJSON()).toMatchSnapshot();
  });
});
