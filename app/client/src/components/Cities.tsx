import React, { FC } from 'react';
import styled from 'styled-components';

import CityComp from '@/components/City';
import Box from '@/components/Box';

import { City } from '@/types';

const Title = styled.h3`
  color: #000000;
  font-size: 30px;
  margin: 0;
  text-align: center;
`;

const Details = styled.div`
  font-size: 15px;
  color: #FFF;
  padding: 20px;
  background-color: rgba(57, 187, 55, 0.63);

  &.empty {
    background-color:rgba(187, 55, 55, 0.63);
  }
`;

const List = styled.div`
  display: flex;
  flex-flow: wrap;
  justify-content: space-around;
  gap: 20px;
`;

export const CitiesDiv = styled(Box)`
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

export type CitiesProps = {
  title: string;
  items: City[];
};

const renderItem = (item: City): React.ReactElement => (
  <CityComp
    {...item}
    key={`${item.codePostal}__${item.codeCommune}`}
  />
);

const Cities:FC<CitiesProps> = ({ items, title }) => (
  <CitiesDiv className='container'>
    <Title>{title}</Title>
    {
      items.length
        ? <Details>{items.length} villes correspondant au texte saisi</Details>
        : <Details className='empty'>Aucune ville correspondant au texte saisi</Details>
    }

    <List>
      {items.map(renderItem)}
    </List>
  </CitiesDiv>
);

export default Cities;
