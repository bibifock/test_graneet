import { create } from 'react-test-renderer';
import 'jest-styled-components';

import City from './City';

describe('components/city', () => {
  it('should render correctly', () => {
    const tree = create(
      <City nomCommune='Maurecourt' codePostal='78780' />
    );

    expect(tree.toJSON()).toMatchSnapshot();
  });
});

