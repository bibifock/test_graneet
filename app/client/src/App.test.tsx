import { create, act } from 'react-test-renderer';
import 'jest-styled-components';

import City from '@/components/City';

import App from './App';

const wait = (delay = 0) =>
  new Promise((resolve) => setTimeout(resolve, delay));

describe('App', () => {
  it('should render a full list of components', async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore: Unreachable code error
    fetch.mockResponse(JSON.stringify({
      cities: [{
        "codePostal":"10200",
        "codeCommune":"10002",
        "nomCommune":"Ailleville",
        "libelleAcheminement":"AILLEVILLE"
      }, {
        "codePostal":"10160",
        "codeCommune":"10003",
        "nomCommune":"Aix-Villemaur-Pâlis",
        "libelleAcheminement":"AIX-VILLEMAUR-PALIS"
      }, {
        "codePostal":"10190",
        "codeCommune":"10003",
        "nomCommune":"Aix-Villemaur-Pâlis",
        "libelleAcheminement":"AIX-VILLEMAUR-PALIS"
      }]
    }));

    const tree = create(<App />);

    expect(tree.root.findAllByType(City)).toHaveLength(0);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore: Unreachable code error
    await act(wait);

    expect(tree.root.findAllByType(City)).toHaveLength(3);

    expect(tree.toJSON()).toMatchSnapshot();
  });
});
