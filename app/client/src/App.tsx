import { useState, useEffect, useCallback } from 'react'
import styled from 'styled-components';

import Cities, { CitiesDiv } from '@/components/Cities';
import Search from '@/components/Search';

import { City } from './types';

const AppDiv = styled.div`
  margin: 20px;
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

const Content = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 20px;

  ${CitiesDiv} {
    flex-grow: 1;
  }
`;

type CitiesList = {
  metro: City[],
  oversea: City[]
};

type GetCities = (search?: string) => Promise<CitiesList>;

const getCities: GetCities = search => fetch(
  '/api' + (search ? `?s=${search}` : '')
).then(r => r.json())
 .then(
   d => d.cities.reduce(
     (o: CitiesList, i: City) => {
       const isOversea = /^9[78]/i.test(i.codePostal);
       o[isOversea ? 'oversea' : 'metro'].push(i);

       return o;
     },
     { oversea: [], metro: [] }
   )
 );

function App() {
  const [cities, setCities] = useState<CitiesList>({ oversea: [], metro: [] });

  const onSearch = useCallback(
    (search?: string) => getCities(search).then(setCities),
    []
  );

  useEffect(() => {
    onSearch();
  }, [])

  return (
    <AppDiv>
      <Search onSearch={onSearch}/>

      <Content>
        <Cities
          title='Villes de métropole'
          items={cities.metro}
        />

        <Cities
          title={`Villes d'outremer`}
          items={cities.oversea}
        />
      </Content>
    </AppDiv>
  )
}

export default App
