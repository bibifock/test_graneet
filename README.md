# Graneet Test technique

## Installation && launch
on root dir

```
npm install
npm run install:all

npm run start
```

then just go [here](http://localhost:3003)

For information port used for this application are:
- 3003: client
- 3004: server
- 5432: postgres

## Tests

on root dir.
```
# Beware you should run database before launch end to end test
npm run start:database
npm run validate
```

This command will launch:
- npm run validate:client -> test and build
- npm run validate:server -> lint, test e2e and build


# Statement

## Exercice de codage
### But de l'exercice
1. Voir les compétences de dev du candidat
2. Voir son aisance sur un projet

### Livrables attendus
1. Un repo avec un Back (idéalement
NestJS
mais pas obligatoire) et un Front
React
2. Une base de donnée est présente et elle est utilisée
3. Une documentation succinte pour installer / lancer le projet
4. Bonus : un déploiement est mis en place
5. Bonus : des tests sont en place

### Enoncé de l'exercice
Mettre en place une application composée d'un front en React consommant une API fournit par un back en NestJS (idéalement mais pas obligatoire).
Cette application permet de lister les villes qui correspondent à une chaîne de caractères en entrée.
Les données sont à récupérer de data.gouv.fr
(https://www.data.gouv.fr/fr/datasets/codes-postaux/)

#### Specs :
Seules les 100 premières villes doivent être affichées.
Les villes doivent-être découpées en deux catégories, celles de la métropole et les autres.
Les villes doivent être ordonnées d'après leurs noms

Le filtrage des villes doit être fait dans le back pour éviter de faire transiter trop de données.

### Design à suivre tant que possible :
https://www.figma.com/file/pKY27LgNp0H54OChEETMVA/Exo-codage-dev

### Ordre de réalisation à privilégier :
- API Back
- Client Front
- Gestion des données dans une base
- BONUS : Déploiement sur un Heroku ou similaire
- BONUS : Mise en place de tests back
- BONUS : Mise en place de tests front
